<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/register', function () {
    return redirect('login');
})->name('register');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/statistic', 'HomeController@getStatistic');
Route::group(['prefix' => 'contact', 'middleware' => 'auth'], function () {
    Route::get('/', 'ContactController@index');
});
Route::group(['prefix' => 'portfolio', 'middleware' => 'auth'], function () {
    Route::resource('/', 'PortfolioController');
});
