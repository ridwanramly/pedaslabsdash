<?php

namespace App\Services;

use App\Contact;
use App\Transformers\ContactTransfromer;

class ContactService
{
    public function index()
    {
        $contact = Contact::all();
        return fractal()
            ->collection($contact)
            ->transformWith(new ContactTransfromer())
            ->toArray();
    }
    public function store($name, $email, $phone, $enquiry)
    {
        $contact = new Contact;
        $save    = $contact->create([
            'name'    => $name,
            'email'   => $email,
            'phone'   => $phone,
            'enquiry' => $enquiry,
        ]);

        return fractal()
            ->item($save)
            ->transformWith(new ContactTransfromer())
            ->toArray();
    }
}
