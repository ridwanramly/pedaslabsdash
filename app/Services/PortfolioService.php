<?php

namespace App\Services;

use App\Portfolio;
use App\Transformers\PortfolioTransformer;

class PortfolioService
{
    public function index()
    {
        $porfolio = Portfolio::all();
        return fractal()
            ->collection($portfolio)
            ->transformWith(new PortfolioTransformer)
            ->toArray();
    }
    public function view($id)
    {
        $portfolio = Portfolio::find($id);
        return fractal()
            ->item($portfolio)
            ->transformWith(new PortfolioTransformer)
            ->toArray();
    }
    public function add($name, $description, $url, $image)
    {
        $portfolio = new Portfolio();
        if ($image->hasFile('image')) {
            # code...
            $file = $image->image;
            $name = md5(rand(0, 12345098762)).'.'.$file->getClientOriginalExtension();
            $file->store('app/public/portfolio', $name);
        }
        $response = $portfolio->create([
            'name'        => $name,
            'description' => $description,
            'url'         => $url,
            'image'       => $name,
        ]);
        return fractal()
            ->item($portfolio)
            ->transformWith(new PortfolioTransformer)
            ->toArray();
    }
}
