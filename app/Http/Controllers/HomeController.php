<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function getStatistic()
    {
        $contact   = \App\Contact::all();
        $portfolio = \App\Portfolio::all();

        $data = [
            'contact'   => $contact->count(),
            'portfolio' => $portfolio->count(),
            'customer'  => 0,
            'sales'     => 0,
        ];
        return response()->json($data);
    }
}
