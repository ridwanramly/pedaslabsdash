<?php

namespace App\Transformers;

use App\Portfolio;
use League\Fractal\TransformerAbstract;

class PortfolioTransfromer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Portfolio $portfolio)
    {
        return [
            'name'        => $portfolio->name,
            'decsription' => $portfolio->decsription,
            'url'         => $portfolio->url,
            'image'       => $portfolio->image,
        ];
    }
}
