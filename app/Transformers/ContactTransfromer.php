<?php

namespace App\Transformers;

use App\Contact;
use League\Fractal\TransformerAbstract;

class ContactTransfromer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Contact $contact)
    {
        return [
            'name'    => $contact->name,
            'email'   => $contact->email,
            'phone'   => $contact->phone,
            'enquiry' => $contact->enquiry,
        ];
    }
}
