
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('home', require('./screen/Home.vue'));

Vue.use(VueRouter);
const routes = [
	{ path: '/', component: require('./screen/Dashboard.vue') },
	{ path: '/contact', component: require('./screen/Contact.vue') }
];
const router = new VueRouter({
	routes
})

const app = new Vue({
    router
}).$mount('#app');
